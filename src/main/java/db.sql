DROP DATABASE IF EXISTS negozio_hibernate;
CREATE DATABASE negozio_hibernate;
USE negozio_hibernate;

CREATE TABLE prodotto(
	prodottoID INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    codice VARCHAR(50) NOT NULL UNIQUE,
    nome VARCHAR(250) NOT NULL,
    prezzo FLOAT
);