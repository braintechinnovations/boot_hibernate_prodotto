package corso.lez20.SpringHibernateProdotto.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import corso.lez20.SpringHibernateProdotto.models.Prodotto;
import corso.lez20.SpringHibernateProdotto.services.ProdottoService;

@RestController
@RequestMapping("/prodotto")
@CrossOrigin("*")
public class ProdottoController {
	
	@Autowired
	private ProdottoService service;

	@PostMapping("/inserisci")
	public Prodotto inserisciProdotto(@RequestBody Prodotto objProd) {
		return service.insert(objProd);
	}
	
	@GetMapping("/{prodotto_id}")				// http://localhost:8085/prodotto/6
	public Prodotto ricercaProdottoPerId(@PathVariable(name="prodotto_id") int idProdotto) {
		return service.findById(idProdotto);
	}
	
	@GetMapping("/")							// http://localhost:8085/prodotto/
	public List<Prodotto> ricercaTuttiProdotti(){
		return service.findAll();
	}
	
	@DeleteMapping("/{prodotto_id}")
	public boolean eliminaProdotto(@PathVariable(name="prodotto_id") int idProdotto) {
		return service.delete(idProdotto);
	}
	
	@PutMapping("/modifica")
	public boolean modificaProdotto(@RequestBody Prodotto objProd) {
		return service.update(objProd);
	}
	
}
