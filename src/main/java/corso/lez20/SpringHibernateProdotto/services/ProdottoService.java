package corso.lez20.SpringHibernateProdotto.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import corso.lez20.SpringHibernateProdotto.models.Prodotto;
import corso.lez20.SpringHibernateProdotto.repositories.DataAccessRepo;

@Service
public class ProdottoService implements DataAccessRepo<Prodotto> {
	
	@Autowired
	private EntityManager ent_man;
	
	private Session getSessione() {
		return ent_man.unwrap(Session.class);
	}

	/**
	 * Inserimento di un nuovo oggetto tramite la creazione di un oggetto temporaneo
	 * nel quale riverso i dati dell'oggetto originale. Nel caso l'operazione di salvataggio vada a buon fine
	 * restituisco l'ggetto.
	 * @param objProd	Oggetto originale
	 * @return	Oggetto con ID assegnato | null
	 */
	public Prodotto insert(Prodotto objProd) {
		
		Prodotto temp = new Prodotto();
		temp.setCodice(objProd.getCodice());
		temp.setNome(objProd.getNome());
		temp.setPrezzo(objProd.getPrezzo());
		
		Session sessione = getSessione();	
		
		try {
			sessione.save(temp);
			return temp;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		} finally {
			sessione.close();						//Superfluo perché gestito da Spring
		}
		
	}
	
//	public Prodotto findById(int id) {
//		
//		Session sessione = getSessione();
//		
//		try {
//			Prodotto temp = sessione.load(Prodotto.class, id);
//			return temp;
//		} catch (Exception e) {
//			System.out.println(e.getMessage());
//			return null;
//		} finally {
//			sessione.close();
//		}
//	}
	
	/**
	 * Ricerca di un oggetto tramite ID, il criteria applica delle restrizioni sulla classe prodotto,
	 * in particolare restituisce un "uniqueResult" se va a buon fine, altrimenti "null" (specificato come Restriction).
	 * @param varId intero
	 * @return Oggetto trovato | null
	 */
	public Prodotto findById(int varId) {
		
		Session sessione = getSessione();
		
//		return (Prodotto) sessione.createCriteria(Prodotto.class)
//				.add(Restrictions.eqOrIsNull("id", varId))
//				.uniqueResult();cq
		
		CriteriaBuilder cb = sessione.getCriteriaBuilder();
		CriteriaQuery<Prodotto> cr = cb.createQuery(Prodotto.class);
		Root<Prodotto> prodotto = cr.from(Prodotto.class);
		cr.select(prodotto).where(cb.gt(prodotto.get("id"), varId));
		

Query<Prodotto> query = sessione.createQuery(cr);
		

        return (Prodotto) query.getResultList();
		
		}
	
	/**
	 * Lista di prodotti corrispondenti al createCriteria (riferimento a Prodotto.class) sotto forma di Lista.
	 * @return List di prodotto, vuota se non trova niente
	 */
	public List<Prodotto> findAll(){
		
		Session sessione = getSessione();
//		return sessione.createCriteria(Prodotto.class).list();
		
		CriteriaQuery<Prodotto> cq = sessione.getCriteriaBuilder().createQuery(Prodotto.class);
        cq.from(Prodotto.class);
        List<Prodotto> prodotti = sessione.createQuery(cq).getResultList();
        return prodotti;
	}
	
	@Transactional
	public boolean delete(int varId) {
		
		Session sessione = getSessione();

		try {
			
			Prodotto temp = sessione.load(Prodotto.class, varId);
			sessione.delete(temp);
			sessione.flush();
			
			return true;
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			
			return false;
		} finally {
			sessione.close();
		}
	}
	
	@Transactional
	public boolean update(Prodotto objProd) {
		
		Session sessione = getSessione();
		
		try {
			
			Prodotto temp = sessione.load(Prodotto.class, objProd.getId());
			
			if(objProd.getCodice() != null)
				temp.setCodice(objProd.getCodice());
			if(objProd.getNome() != null)
				temp.setNome(objProd.getNome());
			if(objProd.getPrezzo() != null)
				temp.setPrezzo(objProd.getPrezzo());
			
			sessione.save(temp);
			sessione.flush();
			
			return true;
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			
			return false;
		} finally {
			sessione.close();
		}
		
	}
	
}
