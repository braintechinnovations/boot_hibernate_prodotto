package corso.lez20.SpringHibernateProdotto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringHibernateProdottoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringHibernateProdottoApplication.class, args);
	}

}
